# Changelog (ebook)

## December 19, 2019
- Many small edits and fixes to the book contents (thanks editors and readers for reporting issues!)
- Updated build system, integrated font subsetting and weighted fonts
- Many CSS tweaks for better compatability
- Indent some short paragraphs after a scene break
- Disabled script-powered hanging punctuation

## April 5, 2019
- Enable hyphenation in paragraphs

## January 18, 2019
- Small typo/grammar fixes

## March 23, 2018
- Fix small-caps for older reading systems

## December 12, 2017
- Release!
